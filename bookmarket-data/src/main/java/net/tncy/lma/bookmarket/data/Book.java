package net.tncy.lma.bookmarket.data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import net.tncy.lma.validator.ISBN;


@Entity // pour indiquer à JPA que cette classe est persistée
@Table(
        name = "BOOKS",
        uniqueConstraints = {
                @UniqueConstraint(name = "BOOK_UNIQUE_ISBN", columnNames = "ISBN")
        }
)
/*@NamedQueries({
        @NamedQuery(name = Book.FIND_ALL, query = "SELECT b from Book b"),
        @NamedQuery(name = Book.FIND_BY_ISBN, query = "SELECT b from Book b WHERE b.isbn = :isbn")
})*/
public class Book {

    /*private static final String FIND_ALL = "Book.FIND_ALL";
    private static final String FIND_BY_ISBN = "Book.FIND_BY_ISBN";*/

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank
    @NotNull
    private String title;
    @NotBlank
    @NotNull
    private String author;

    private String publisher;
    private BookFormat format;

    @NotBlank
    @NotNull
    @ISBN
    private String isbn;


   public Book(String title, String author, String publisher, BookFormat format, String isbn) {
       this.title = title;
       this.author = author;
       this.publisher = publisher;
       this.format = format;
       this.isbn = isbn;
   }
}
