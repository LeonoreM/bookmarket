package net.tncy.lma.bookmarket.data;

import javax.persistence.Entity;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class InventoryEntry {

    @NotBlank
    @NotNull
    private Book book;

    @NotBlank
    @NotNull
    private int quantity;

    private float averagePrice;
    private float currentPrice;
}
