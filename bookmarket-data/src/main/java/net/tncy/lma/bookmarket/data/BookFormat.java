package net.tncy.lma.bookmarket.data;

public enum BookFormat {

    BROCHE,
    POCHE
}
