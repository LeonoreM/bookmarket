package net.tncy.lma.bookmarket.services;

import net.tncy.lma.bookmarket.data.Bookstore;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/bookstore")
public class BookstoreService {

    @GET
    @Path("/random")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Bookstore getRandom() {
        Bookstore bookstore = new Bookstore();
        return bookstore;
    }

}
