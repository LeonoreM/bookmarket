package net.tncy.lma.bookmarket.services;

import net.tncy.lma.bookmarket.data.Book;
import net.tncy.lma.bookmarket.data.BookFormat;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


@Path("/book")
public class BookService {

    /*private final static String PERSISTENCE_UNIT_NAME = "BOOKMARKET-PU";

    @PersistenceContext(unitName = PERSISTENCE_UNIT_NAME)
    private EntityManager em;*/

    @GET
    @Path("/random/{publisher}")
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Book getRandom(@PathParam("publisher") String publisher) {
        /*Query query = em.createQuery("select b from Book b");
        return query.getResultList();*/
        return new Book("Les Liaisons Dangereuses","Choderlos de Laclos",publisher, BookFormat.POCHE,"06gfdg");
    }
}
